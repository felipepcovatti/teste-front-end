import { format } from 'date-fns'

export const formatDate = (date: any) => {
  const parsedDate = Date.parse(date);

  if (!parsedDate) return ''

  return format(parsedDate, "dd/MM/yyyy")
}