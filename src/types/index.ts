export type Form = {
  id: string;
  int_total_answers: number;
  str_name: string;
  str_description: string;
  str_publication_type: string | null;
  ts_created_at: string;
  ts_published_at: string;
  ts_publication_start: string;
  ts_publication_end: string;
  lst_tags: string[];
};
