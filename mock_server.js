const { json } = require('express')
const cors = require('cors')
const express = require('express')
const data = require('./src/assets/mock_data.json')
const app = express()
const port = 3000

app.use(json())

app.use(cors({
  exposedHeaders: 'x-total-count'
}))

app.get('/', (req, res) => {
  const { status } = req.query
  const page = Number(req.query.page)
  const limit = Number(req.query.limit)

  const itemsFilteredByStatus = data.filter(item => {
    if (status === 'published') {
      return item.str_publication_type !== null
    }
    if (status === 'draft') {
      return item.str_publication_type === null
    }
    return true
  })

  const totalItemsCount = itemsFilteredByStatus.length

  res.setHeader('X-Total-Count', totalItemsCount)

  const firstItemIndex = (page - 1) * limit;

  const slice = itemsFilteredByStatus.slice(firstItemIndex, firstItemIndex + limit)

  return res.json(slice)
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})